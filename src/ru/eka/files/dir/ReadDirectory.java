package ru.eka.files.dir;

import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 */
public class ReadDirectory {
    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        File pathDirectory = new File(inputWay("src\\ru\\eka\\files\\dir\\inWay.txt"));
        String[] listDir = pathDirectory.list();

        System.out.println("Какое слово нужно найти? - ");
        String searchText = scan.next();
        System.out.println("В данной директории хранятся: " + Arrays.toString(listDir));

        File[] files = pathDirectory.listFiles();
        assert files != null;

        for (File file : files) {
            search(file, searchText);
        }
    }

    /**
     * Метод для поиска текста и записи его в файл
     *
     * @param file файл
     * @param searchText текст для поиска
     */
    private static void search(File file, String searchText) {
        String string;
        int count = 0;
        try (BufferedReader reader = new BufferedReader(new FileReader(file));
             BufferedWriter writer = new BufferedWriter(new FileWriter("src\\ru\\eka\\files\\dir\\result.txt",true))) {
            while ((string = reader.readLine()) != null) {
                    count++;
                if (string.contains(searchText)) {
                    writer.write("Текст найден в файле " + file.getName() + " в " + count + " строке " + ", который находится " + file.getParent() + "\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }




    /**
     * Метод для считывания пути из файла
     *
     * @param string файл
     * @return путь
     */
    private static String inputWay(String string) {
        StringBuilder way = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(string))) {
            String s;
            while ((s = reader.readLine()) != null) {
                way.append(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return way.toString();
    }
}
